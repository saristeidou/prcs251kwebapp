﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestApp.Models;
/// <summary>
/// This 
/// </summary>
namespace TestApp.Controllers
{
    public class CartItem
    {
        private ITEM_SIZE pr = new ITEM_SIZE();

        public ITEM_SIZE Pr
        {
            get { return pr; }
            set { pr = value; }
        }

    

        private int quantity;

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public CartItem()
        { }
        public CartItem(ITEM_SIZE pr, int quantity)
        {
            this.pr = pr;
            this.quantity = quantity;
            
        }
      
    }
}