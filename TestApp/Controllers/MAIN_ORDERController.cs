﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;
using System.Web.Mvc.Async;
using System.Threading.Tasks;

namespace TestApp.Controllers
{
    public class MAIN_ORDERController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: MAIN_ORDER
        public ActionResult Index()
        {
            var mAIN_ORDER = db.MAIN_ORDER.Include(m => m.CUSTOMER);
            return View(mAIN_ORDER.ToList());
        }

        public ActionResult Track()
        {
            var oRDER = db.MAIN_ORDER.Include(m => m.CUSTOMER);
            return View(oRDER.ToList());
        }

        // GET: MAIN_ORDER/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAIN_ORDER mAIN_ORDER = db.MAIN_ORDER.Find(id);
            if (mAIN_ORDER == null)
            {
                return HttpNotFound();
            }
            return View(mAIN_ORDER);
        }

        //public JsonResult GetCurrentState()
        //{
        //    MAIN_ORDER Mo = new MAIN_ORDER();
        //    var CurrentState = Mo.ORDER_STATUS;

        //return JsonResult(CurrentState, JsonRequestBehavior.AllowGet);
        //}
        //public async Task<ActionResult> CurrentState()
        //{
        //    ViewBag.SyncOrAsync = "Asynchronous";
        //    //var gizmoService = new GizmoService();
        //    MAIN_ORDER Mo = new MAIN_ORDER();
        //    var Currentstate = Mo.ORDER_STATUS;
        //    return View("Gizmos", await Currentstate.Index());
        //}

        // GET: MAIN_ORDER/Create
        public ActionResult Create()
        {
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME");
            return View();
        }

        // POST: MAIN_ORDER/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ORDER_ID,ORDER_PRICE,ORDER_STATUS,ORDER_COMMENT,CUSTOMER_ID,ADDRESS,ORDER_DATE,NAME,POST_CODE,VERIFICATION_CODE")] MAIN_ORDER mAIN_ORDER)
        {
            if (ModelState.IsValid)
            {
                db.MAIN_ORDER.Add(mAIN_ORDER);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", mAIN_ORDER.CUSTOMER_ID);
            return View(mAIN_ORDER);
        }

        // GET: MAIN_ORDER/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAIN_ORDER mAIN_ORDER = db.MAIN_ORDER.Find(id);
            if (mAIN_ORDER == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", mAIN_ORDER.CUSTOMER_ID);
            return View(mAIN_ORDER);
        }

        // POST: MAIN_ORDER/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ORDER_ID,ORDER_PRICE,ORDER_STATUS,ORDER_COMMENT,CUSTOMER_ID,ADDRESS,ORDER_DATE,NAME,POST_CODE,VERIFICATION_CODE")] MAIN_ORDER mAIN_ORDER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mAIN_ORDER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", mAIN_ORDER.CUSTOMER_ID);
            return View(mAIN_ORDER);
        }

        // GET: MAIN_ORDER/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAIN_ORDER mAIN_ORDER = db.MAIN_ORDER.Find(id);
            if (mAIN_ORDER == null)
            {
                return HttpNotFound();
            }
            return View(mAIN_ORDER);
        }

        // POST: MAIN_ORDER/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MAIN_ORDER mAIN_ORDER = db.MAIN_ORDER.Find(id);
            db.MAIN_ORDER.Remove(mAIN_ORDER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
