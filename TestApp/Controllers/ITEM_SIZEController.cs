﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class ITEM_SIZEController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: ITEM_SIZE
        public ActionResult Index()
        {
            return View(db.ITEM_SIZE.ToList());
        }

        // GET: ITEM_SIZE/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_SIZE iTEM_SIZE = db.ITEM_SIZE.Find(id);
            if (iTEM_SIZE == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_SIZE);
        }

        // GET: ITEM_SIZE/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ITEM_SIZE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SIZE_ID,UNIT_PRICE")] ITEM_SIZE iTEM_SIZE)
        {
            if (ModelState.IsValid)
            {
                db.ITEM_SIZE.Add(iTEM_SIZE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iTEM_SIZE);
        }

        // GET: ITEM_SIZE/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_SIZE iTEM_SIZE = db.ITEM_SIZE.Find(id);
            if (iTEM_SIZE == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_SIZE);
        }

        // POST: ITEM_SIZE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SIZE_ID,UNIT_PRICE")] ITEM_SIZE iTEM_SIZE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iTEM_SIZE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iTEM_SIZE);
        }

        // GET: ITEM_SIZE/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_SIZE iTEM_SIZE = db.ITEM_SIZE.Find(id);
            if (iTEM_SIZE == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_SIZE);
        }

        // POST: ITEM_SIZE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ITEM_SIZE iTEM_SIZE = db.ITEM_SIZE.Find(id);
            db.ITEM_SIZE.Remove(iTEM_SIZE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
