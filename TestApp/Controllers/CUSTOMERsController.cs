﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;
using System.Web.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace TestApp.Controllers

{
    public class CUSTOMERsController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: CUSTOMERs
        public ActionResult Index()
        {
            return View(db.CUSTOMERs.ToList());
        }

        public ActionResult ChangePass()
        {
            return View();
        }

        public ActionResult ChangePass(string pass, string pass1, string id)
        {
            CUSTOMER a = new CUSTOMER();
            var regexItem = new Regex("^[a-zA-Z0-9 ]+$");

            if (regexItem.IsMatch(pass))
            {
                string random = RandomString(5);
                var UserPass = random + a.PASSWORD;
                string hash = sha256(UserPass);
                a.SALT = random;
                a.PASSWORD = hash;
                db.CUSTOMERs.Add(a);

                db.SaveChanges();

            }
            else
            {
                ModelState.AddModelError("", "Password contains invalid character !@#$%^&*()?");
            }
            return  RedirectToAction("http://localhost:62750/Home/Index");
        }

        // GET: CUSTOMERs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // GET: CUSTOMERs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CUSTOMERs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CUSTOMER_ID,NAME,SURNAME,ADDRESS,POST_CODE,CONTACT_NO,USERNAME,PASSWORD,EMAIL")] CUSTOMER cUSTOMER)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    var regexItem = new Regex("^[a-zA-Z0-9 ]+$");

                    if (regexItem.IsMatch(cUSTOMER.PASSWORD))
                    {
                        string random = RandomString(5);
                        var UserPass = random + cUSTOMER.PASSWORD;
                        string hash = sha256(UserPass);
                        cUSTOMER.SALT = random;
                        cUSTOMER.PASSWORD = hash;
                        db.CUSTOMERs.Add(cUSTOMER);

                        db.SaveChanges();
                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "Password contains invalid character !@#$%^&*()?");
                    }



                    return RedirectToAction("Login", "CUSTOMERs", new { area = "" });



                }
                catch (Exception x)
            {
                ModelState.AddModelError("", "Username or Password Doesn't Exist");
            }

        }
            else
            {


            }

            return View(cUSTOMER);
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(CUSTOMER cUSTOMER)
        {
            using (Entities1 db = new Entities1())
            {
                CUSTOMER a = new CUSTOMER();
                var UserInput = db.CUSTOMERs.FirstOrDefault(b => b.EMAIL == cUSTOMER.EMAIL);
                if (UserInput != null && UserInput.PASSWORD == sha256(UserInput.SALT + cUSTOMER.PASSWORD))
                {
                    try
                    {
                        Session["CustomerId"] = UserInput.CUSTOMER_ID.ToString();
                        //Session["Email"] = UserInput.EMAIL.ToString();
                        Session["Name"] = UserInput.NAME.ToString();
                        //return RedirectToAction("Index");
                        return Redirect("http://localhost:62750/Home/Index");
                    }
                    catch (Exception x)
                    {
                        ModelState.AddModelError("", "Email or Password Doesn't Exist");
                    }
                }
                else
                {
                    
                
                        ModelState.AddModelError("", "Email or password is wrong");
                    }
               
            }
            return View(cUSTOMER);
        }

       

        
        public ActionResult Logout()
        {
            Session.Clear();
            
            return Redirect("http://localhost:62750/Home/Index");
        }
        // GET: CUSTOMERs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: CUSTOMERs/Edit/5
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CUSTOMER_ID,NAME,SURNAME,ADDRESS,POST_CODE,CONTACT_NO,PASSWORD,EMAIL")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("http://localhost:62750/ORDER_HISTORY/Index");
            }
            return View(cUSTOMER);
        }

        // GET: CUSTOMERs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: CUSTOMERs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            db.CUSTOMERs.Remove(cUSTOMER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
