﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace TestApp.Controllers
{
    public class ShoppingCartController : Controller
    {
        int BusketItems = 0;
        double total = 0;
        int i = 0;

        
        
        private Entities1 pz = new Entities1();
       
        public ActionResult Index()
        {
            return View();
        }

        public class Size
        {
            public int Prise { get; set; }
            public string ItemSize { get; set; }
        }
        public IEnumerable<Size> SizeOptions =
       new List<Size>
       {
            new Size {Prise = 0, ItemSize = "Small"},
            new Size {Prise = 3, ItemSize = "Medium"},
            new Size {Prise = 6, ItemSize = "Large"}
       };



        private int isExisting(int id)
        {
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Pr.SIZE_ID == id.ToString())                
                    return i;
                
                
             }
            return -1;
        }

        public ActionResult AddQt(int id)
        {
            int index = isExisting(id);
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            cart[index].Quantity++;
            Session["cart"] = cart;
            total = Convert.ToDouble(Session["Total"]) + Convert.ToDouble(cart[index].Pr.UNIT_PRICE);
            Session["Total"] = total.ToString();
            return View("Cart");
        }
        public ActionResult RemoveQt(int id)
        {
            int index = isExisting(id);
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            cart[index].Quantity--;
            Session["cart"] = cart;
            total = Convert.ToDouble(Session["Total"]) - Convert.ToDouble(cart[index].Pr.UNIT_PRICE);
            Session["Total"] = total.ToString();
            return View("Cart");
        }
        public ActionResult Delete(int id)
        {
            //ITEM_SIZE Is = new ITEM_SIZE();
            int index = isExisting(id);
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            //double price = Convert.ToDouble(Is);
            //int index = cart.IndexOf(id);
            cart.RemoveAt(index);
            Session["cart"] = cart;
            total = Convert.ToDouble(Session["Total"]) - Convert.ToDouble(cart[index].Pr.UNIT_PRICE * cart[index].Quantity);
            Session["Total"] = total.ToString();
            
            
            return View("Cart");


        }

        public ActionResult Busket()
        {

            return View("Cart");
        }

        public ActionResult OrderNow(int id)
        {
            //BusketItems++;
            //Session["BusketItems"] = BusketItems.ToString();
            if (Session["cart"] == null)
            {
                
                ITEM_SIZE Is = new ITEM_SIZE();
                
                    //double price = Convert.ToDouble(Is.UNIT_PRICE);
                    List<CartItem> cart = new List<CartItem>();
                    cart.Add(new CartItem(pz.ITEM_SIZE.Find(id.ToString()), 1));
                    Session["cart"] = cart;
                    BusketItems = 1;
                    Session["BusketItems"] = BusketItems.ToString();
                    total = Convert.ToDouble(cart[0].Pr.UNIT_PRICE);
                    Session["Total"] = total.ToString();
                    //string url = Request.Url.AbsolutePath;
                    //Session["URL"] = url;
                    
                
            }
            else
            {
                //ITEM_SIZE Is = new ITEM_SIZE();
                List<CartItem> cart = (List<CartItem>)Session["cart"];
                int index = isExisting(id);
                //double price = Convert.ToDouble(Is);
                if (index == -1)
                    
                    cart.Add(new CartItem(pz.ITEM_SIZE.Find(id.ToString()), 1));
                else
                    cart[index].Quantity++;

                
                Session["cart"] = cart;
                BusketItems = cart.Count;
                Session["BusketItems"] = BusketItems.ToString();
                i = BusketItems - 1;
                total = Convert.ToDouble(Session["Total"]) + Convert.ToDouble(cart[i].Pr.UNIT_PRICE);
                Session["Total"] = total.ToString();
            }
            
           
            //ScriptManager.RegisterStartupScript( this , this.GetType(), "testScript", "autoclosable - btn - info", true);
            return Redirect(Request.UrlReferrer.ToString()); 
            //Request.UrlReferrer.ToString()
        }

        //[HttpPost]
        
        //public ActionResult Pay(ORDER_DETAIL Order)
        //{
        //    if (Session["CustomerId"] != null)
        //    {
        //        // int test = 299;
        //        List<CartItem> cart = (List<CartItem>)Session["cart"];
        //        for (int i = 0; i < cart.Count; i++)
        //        {

        //            // Order.ORDER_DETAIL_ID = test.ToString();
        //            Order.CUSTOMER_ID = Session["CustomerId"].ToString();
        //            Order.ITEM_ID = cart[i].Pr.ITEM_ID;
        //            Order.QUANTITY = cart[i].Quantity.ToString();
        //            Order.TOTALCOST = cart[i].Pr.UNIT_PRICE* cart[i].Quantity;
        //            pz.ORDER_DETAIL.Add(Order);
        //            pz.SaveChanges();
        //            // test++;

        //        }
        //        return Redirect("http://localhost:62750/ORDER_DETAIL/Index");
        //    }
        //    else
        //    {
        //        return Redirect("http://localhost:62750/CUSTOMERs/Login");
        //    }
            //Order.ORDER_ID = test.ToString();
            //try
            //{
            //    pz.SaveChanges();
            //}
            //catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            //{
            //    Exception raise = dbEx;
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            string message = string.Format("{0}:{1}",
            //                validationErrors.Entry.Entity.ToString(),
            //                validationError.ErrorMessage);
            //            // raise a new exception nesting
            //            // the current instance as InnerException
            //            raise = new InvalidOperationException(message, raise);
            //        }
            //    }
            //    throw raise;
            //}
            

        
        //protected void CheckoutBtn_Click(object sender, ImageClickEventArgs e)
        //{
        //    using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
        //    {
        //        Session["payment_amt"] = usersShoppingCart.GetTotal();
        //    }
        //    Response.Redirect("Checkout/CheckoutStart.aspx");
        //}
    }

}