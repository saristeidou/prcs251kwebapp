﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class ORDER_DETAILController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: ORDER_DETAIL
        public ActionResult Index()
        {
            var oRDER_DETAIL = db.ORDER_DETAIL.Include(o => o.ITEM).Include(o => o.CUSTOMER);
            return View(oRDER_DETAIL.ToList());
        }

        // GET: ORDER_DETAIL/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_DETAIL oRDER_DETAIL = db.ORDER_DETAIL.Find(id);
            if (oRDER_DETAIL == null)
            {
                return HttpNotFound();
            }
            return View(oRDER_DETAIL);
        }

       
        public ActionResult Create(int id)
        {
            ORDER_DETAIL order = new ORDER_DETAIL();
            int qt = 1;
            bool idExists = db.ORDER_DETAIL.Any(ID => ID.ITEM_ID.Equals(order.ITEM_ID));
            if (idExists)
            {
               // qt = db.ORDER_DETAIL.Find();
                return RedirectToAction("Index");
            }
            else
            {
                order.ITEM_ID = id.ToString();
                order.CUSTOMER_ID = Session["CustomerId"].ToString();
                order.QUANTITY = qt;
                //order.ORDER_DETAIL_ID = qt.ToString();
                db.ORDER_DETAIL.Add(order);
                db.SaveChanges();
                Session["Order"]= db.ORDER_DETAIL.OrderByDescending(p => p.ORDER_ID).FirstOrDefault().ORDER_ID;
                return RedirectToAction("Index");
            }
            
        }

        // GET: ORDER_DETAIL/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_DETAIL oRDER_DETAIL = db.ORDER_DETAIL.Find(id);
            if (oRDER_DETAIL == null)
            {
                return HttpNotFound();
            }
            ViewBag.ITEM_ID = new SelectList(db.ITEMs, "ITEM_ID", "ITEM_NAME", oRDER_DETAIL.ITEM_ID);
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", oRDER_DETAIL.CUSTOMER_ID);
            return View(oRDER_DETAIL);
        }

        // POST: ORDER_DETAIL/Edit/5
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ORDER_DETAIL_ID,ITEM_ID,CUSTOMER_ID,QUANTITY,UNIT_PRICE,TOTALCOST,ORDER_ID")] ORDER_DETAIL oRDER_DETAIL)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oRDER_DETAIL).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ITEM_ID = new SelectList(db.ITEMs, "ITEM_ID", "ITEM_NAME", oRDER_DETAIL.ITEM_ID);
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", oRDER_DETAIL.CUSTOMER_ID);
            return View(oRDER_DETAIL);
        }

        // GET: ORDER_DETAIL/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_DETAIL oRDER_DETAIL = db.ORDER_DETAIL.Find(id);
            if (oRDER_DETAIL == null)
            {
                return HttpNotFound();
            }
            return View(oRDER_DETAIL);
        }

        // POST: ORDER_DETAIL/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ORDER_DETAIL oRDER_DETAIL = db.ORDER_DETAIL.Find(id);
            db.ORDER_DETAIL.Remove(oRDER_DETAIL);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
