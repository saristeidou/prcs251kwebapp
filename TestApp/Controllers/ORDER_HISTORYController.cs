﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class ORDER_HISTORYController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: ORDER_HISTORY
        public ActionResult Index()
        {
            var oRDER_HISTORY = db.ORDER_HISTORY.Include(o => o.CUSTOMER);
            return View(oRDER_HISTORY.ToList());
        }
        //public ActionResult Track()
        //{
        //    var oRDER = db.ORDER_HISTORY.Include(m => m.CUSTOMER);
        //    return View(oRDER.ToList());
        //}
        // GET: ORDER_HISTORY/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_HISTORY oRDER_HISTORY = db.ORDER_HISTORY.Find(id);
            if (oRDER_HISTORY == null)
            {
                return HttpNotFound();
            }
            return View(oRDER_HISTORY);
        }

        // GET: ORDER_HISTORY/Create
        public ActionResult Create()
        {
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME");
            return View();
        }

        // POST: ORDER_HISTORY/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ORDER_HISTORY_ID,ORDER_ID,CUSTOMER_ID,TOTALCOST,ORDER_DATE,RIDER_ID")] ORDER_HISTORY oRDER_HISTORY)
        {
            if (ModelState.IsValid)
            {
                db.ORDER_HISTORY.Add(oRDER_HISTORY);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", oRDER_HISTORY.CUSTOMER_ID);
            return View(oRDER_HISTORY);
        }

        // GET: ORDER_HISTORY/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_HISTORY oRDER_HISTORY = db.ORDER_HISTORY.Find(id);
            if (oRDER_HISTORY == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", oRDER_HISTORY.CUSTOMER_ID);
            return View(oRDER_HISTORY);
        }

        // POST: ORDER_HISTORY/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ORDER_HISTORY_ID,ORDER_ID,CUSTOMER_ID,TOTALCOST,ORDER_DATE,RIDER_ID")] ORDER_HISTORY oRDER_HISTORY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oRDER_HISTORY).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUSTOMER_ID = new SelectList(db.CUSTOMERs, "CUSTOMER_ID", "NAME", oRDER_HISTORY.CUSTOMER_ID);
            return View(oRDER_HISTORY);
        }

        // GET: ORDER_HISTORY/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDER_HISTORY oRDER_HISTORY = db.ORDER_HISTORY.Find(id);
            if (oRDER_HISTORY == null)
            {
                return HttpNotFound();
            }
            return View(oRDER_HISTORY);
        }

        // POST: ORDER_HISTORY/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ORDER_HISTORY oRDER_HISTORY = db.ORDER_HISTORY.Find(id);
            db.ORDER_HISTORY.Remove(oRDER_HISTORY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
